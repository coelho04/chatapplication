﻿namespace Stock;

public interface IStockService
{
    Task<StockValue> GetStockValue(string code);
}