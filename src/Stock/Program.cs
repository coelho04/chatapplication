using System.Text;
using System.Text.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Stock;

var builder = Host.CreateApplicationBuilder(args);
builder.Services.AddHttpClient<IStockService, StockService>(
    client =>
    {
        // Set the base address of the typed client.
        client.BaseAddress = new Uri("https://stooq.com/q/l/?f=sd2t2ohlcv&h&e=csv");

        // Add a user-agent default request header.
        client.DefaultRequestHeaders.UserAgent.ParseAdd("net challenge");
    });

// https://stooq.com/q/l/?s=aapl.us&f=sd2t2ohlcv&h&e=csv

var host = builder.Build();

var factory = new ConnectionFactory { HostName = "localhost" }; 
var connection = factory.CreateConnection(); 
using var channel = connection.CreateModel();
channel.QueueDeclare("stock.input",
    durable: false,
    exclusive: false,
    autoDelete: false,
    arguments: null);

channel.QueueDeclare("stock.output",
    durable: false,
    exclusive: false,
    autoDelete: false,
    arguments: null);

var consumer = new EventingBasicConsumer(channel);

consumer.Received += (model, ea) =>
{
    var body = ea.Body.ToArray();
    var code = Encoding.UTF8.GetString(body);

    var stockService = host.Services.GetRequiredService<IStockService>();
    var result = stockService.GetStockValue(code).Result;
    
    var json = JsonSerializer.Serialize(result);
    channel.BasicPublish(exchange: "", routingKey: "stock.output", body: Encoding.UTF8.GetBytes(json));
};

channel.BasicConsume(queue: "stock.input",
    autoAck: true,
    consumer: consumer);

host.Run();