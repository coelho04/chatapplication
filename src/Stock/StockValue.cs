﻿using CsvHelper.Configuration.Attributes;

namespace Stock;

public record StockValue(
    [Index(0)]string Symbol, 
    [Index(1)]DateTime Date, 
    [Index(3)]decimal Open, 
    [Index(4)]decimal High, 
    [Index(5)]decimal Low, 
    [Index(6)]decimal Close);