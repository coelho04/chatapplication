﻿using System.Globalization;
using CsvHelper;

namespace Stock;

public class StockService(HttpClient client) : IStockService
{
    public async Task<StockValue> GetStockValue(string code)
    {
        code = "aapl.us";
        var response = await client.GetAsync($"s={code}");
        
        response.EnsureSuccessStatusCode();
        
        var stream = await response.Content.ReadAsStreamAsync();
        using var reader = new StreamReader(stream);
        using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
        return csv.GetRecord<StockValue>();
    }
}