﻿using System.Text;
using System.Text.Json;
using CrossCutting;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.JSInterop;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Client.Pages
{
    public partial class Chat
    {
        private IModel? _channel;

        private AsyncEventingBasicConsumer? _consumer;
        
        private const string StockCommand = "/stock=";
        
        [CascadingParameter] public HubConnection hubConnection { get; set; }
        [Parameter] public string CurrentMessage { get; set; }
        [Parameter] public string CurrentUserId { get; set; }
        [Parameter] public string CurrentUserEmail { get; set; }
        private List<ChatMessage> messages = new List<ChatMessage>();
        private async Task SubmitAsync()
        {
            if (!string.IsNullOrEmpty(CurrentMessage) && !string.IsNullOrEmpty(ContactId))
            {
                if (CurrentMessage.StartsWith(StockCommand))
                {
                    var code =  CurrentMessage[StockCommand.Length..];
                    if (string.IsNullOrWhiteSpace(code))
                    {
                        await hubConnection.SendAsync("SendMessageAsync", "Invalid Stock Code", CurrentUserEmail);
                    }
                    else
                    {
                        var json = JsonSerializer.Serialize(code);
                        _channel?.BasicPublish(exchange: "", routingKey: "stock.input", body: Encoding.UTF8.GetBytes(json));
                    }
                    
                    CurrentMessage = string.Empty;
                }
                
                //Save Message to DB
                var chatHistory = new ChatMessage()
                {
                    Message = CurrentMessage,
                    ToUserId = ContactId,
                    CreatedDate = DateTime.Now

                };
                await _chatManager.SaveMessageAsync(CurrentMessage, ContactId);
                chatHistory.FromUserId = CurrentUserId;
                await hubConnection.SendAsync("SendMessageAsync", chatHistory, CurrentUserEmail);
                CurrentMessage = string.Empty;
            }
        }
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await _jsRuntime.InvokeAsync<string>("ScrollToBottom", "chatContainer");
        }
        protected override async Task OnInitializedAsync()
        {
            if (hubConnection == null)
            {
                hubConnection = new HubConnectionBuilder().WithUrl(_navigationManager.ToAbsoluteUri("/signalRHub")).Build();
            }
            if (hubConnection.State == HubConnectionState.Disconnected)
            {
                await hubConnection.StartAsync();
            }
            hubConnection.On<ChatMessage, string>("ReceiveMessage", async (message, userName) =>
            {
                if ((ContactId == message.ToUserId && CurrentUserId == message.FromUserId) || (ContactId == message.FromUserId && CurrentUserId == message.ToUserId))
                {

                    if ((ContactId == message.ToUserId && CurrentUserId == message.FromUserId))
                    {
                        messages.Add(new ChatMessage { Message = message.Message, CreatedDate = message.CreatedDate, FromUser = new ApplicationUser() { Email = CurrentUserEmail } });
                        await hubConnection.SendAsync("ChatNotificationAsync", $"New Message From {userName}", ContactId, CurrentUserId);
                    }
                    else if ((ContactId == message.FromUserId && CurrentUserId == message.ToUserId))
                    {
                        messages.Add(new ChatMessage { Message = message.Message, CreatedDate = message.CreatedDate, FromUser = new ApplicationUser() { Email = ContactEmail } });
                    }
                    await _jsRuntime.InvokeAsync<string>("ScrollToBottom", "chatContainer");
                    StateHasChanged();
                }
            });
            await GetUsersAsync();
            var state = await _stateProvider.GetAuthenticationStateAsync();
            var user = state.User;
            CurrentUserId = user.Claims.Where(a => a.Type == "sub").Select(a => a.Value).FirstOrDefault();
            CurrentUserEmail = user.Claims.Where(a => a.Type == "name").Select(a => a.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(ContactId))
            {
                await LoadUserChat(ContactId);
            }

            // Couldn't get this to work on time
            try
            {
                var factory = new ConnectionFactory { HostName = "localhost" };
                var connection = factory.CreateConnection();
                this._channel = connection.CreateModel();
        
                _channel.QueueDeclare("stock.input",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                _channel.QueueDeclare("stock.output",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);
            
                _consumer = new AsyncEventingBasicConsumer(_channel);
            
                _channel.BasicConsume(queue: "stock.output",
                    autoAck: true,
                    consumer: _consumer);
            
                _consumer.Received+= ReceivedStockValue;
            }
            catch (Exception exception)
            {
                // ignored
            }
        }
        public List<ApplicationUser> ChatUsers = new List<ApplicationUser>();
        [Parameter] public string ContactEmail { get; set; }
        [Parameter] public string ContactId { get; set; }
        async Task LoadUserChat(string userId)
        {
            var contact = await _chatManager.GetUserDetailsAsync(userId);
            ContactId = contact.Id;
            ContactEmail = contact.Email;
            _navigationManager.NavigateTo($"chat/{ContactId}");
            messages = new List<ChatMessage>();
            messages = await _chatManager.GetConversationAsync(ContactId);
        }
        private async Task GetUsersAsync()
        {
            ChatUsers = await _chatManager.GetUsersAsync();
        }
        
        
        private void RequestStockValue(string code)
        {
            var json = JsonSerializer.Serialize(code);
            var body = Encoding.UTF8.GetBytes(json);

            _channel.BasicPublish(exchange: "", routingKey: "stock.input", body: body);
        }
        
        private async Task ReceivedStockValue(object? model, BasicDeliverEventArgs ea)
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            var stockValue = JsonSerializer.Deserialize<StockValue>(message);
            
            await hubConnection.SendAsync("SendMessageAsync", $"{stockValue.Symbol} quote is ${stockValue.Close} per share", CurrentUserEmail);
            
        }
    }
}

public record StockValue(
    string Symbol, 
    DateTime Date, 
    decimal Open, 
    decimal High, 
    decimal Low, 
    decimal Close);
