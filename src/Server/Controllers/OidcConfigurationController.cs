﻿using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Mvc;

namespace Server.Controllers
{
    public class OidcConfigurationController(IClientRequestParametersProvider clientRequestParametersProvider)
        : Controller
    {
        private IClientRequestParametersProvider ClientRequestParametersProvider { get; } = clientRequestParametersProvider;

        [HttpGet("_configuration/{clientId}")]
        public IActionResult GetClientRequestParameters([FromRoute] string clientId)
        {
            var parameters = ClientRequestParametersProvider.GetClientParameters(HttpContext, clientId);
            return Ok(parameters);
        }
    }
}
